<?php
// Buatlah class Animal tersebut di dalam file animal.php.Lakukan instance terhadap class Animal tersebut di file index.php. Lakukan import class Animal dari animal.php di dalam index.php menggunakan require atau require 


$sheep = new Animal("shaun");
 class Animal {
     public  $name = "shaun";
     public $legs = 4;
     public $cold_blooded = "no";
     
 }

// echo $sheep->name ; // "shaun"
// echo $sheep->legs ; // 4
// echo $sheep->cold_blooded; // "no"


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>