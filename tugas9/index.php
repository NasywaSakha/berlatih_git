<?php
// index.php
require_once('animal.php');
$sheep = new Animal("shaun");
echo "Name : " . $sheep->name."<br>";
echo "legs : " . $sheep->legs."<br>";
echo "cold blooded : " . $sheep-> cold_blooded . "<br>";

require_once('frog.php');
$kodok = new Frog("buduk");
$kodok->jump("hop hop") ;  
echo "Name : " . $kodok->name."<br>";
echo "legs : " . $kodok->legs."<br>";
echo "cold blooded : " . $kodok-> cold_blooded . "<br>";
echo "Jump : " . $kodok->jump()."<br>";

require_once('ape.php');
$sungokong = new Ape("kera sakti");
$sungokong->yell("Auooo"); 
echo "Name : " . $sungokong->name."<br>";
echo "legs : " . $sungokong->legs ."<br>";
echo "cold blooded : " . $sungokong-> cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell()."<br>";
?>